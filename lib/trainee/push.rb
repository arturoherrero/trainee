class Push
  ISSUE = ENV["TRAINEE_MAINTAINER_ISSUE"]

  def initialize
    @draft = Draft.new
  end

  def call
    draft.entries.each do |entry|
      Gitlab.create_issue_note("gitlab-com/www-gitlab-com", ISSUE, entry)
    end

    draft.delete
  end

  private

  attr_reader :draft
end
