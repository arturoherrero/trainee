# Trainee

A tool to help me with my [Trainee BE maintainer (GitLab) issue][3].

This project is heavily inspired by [Traintainer][1] and [ReviewTanuki][2].


## Usage

### Setup

#### Clone the project

Clone the project locally:

```sh
git clone git@gitlab.com:arturoherrero/trainee.git
```

Install dependecies:

```sh
bundle install
```

#### Edit `.env` file

Edit `.env` file to set the following environment variables:

- `GITLAB_API_PRIVATE_TOKEN`: Your API access token.
- `TRAINEE_MAINTAINER_ISSUE`: Your trainee maintainer issue id on [gitlab-com/www-gitlab-com][4].
- `MERGE_REQUEST_EMOJI`: The emoji that you are going to use reviewing merge requests.

NOTE: You can ignore files already managed with Git locally:

```sh
git update-index --skip-worktree .env
```

#### Update `PATH`

To find `trainee` command, update your `PATH` in `~/.bash_profile`:

```bash
export PATH="~/path/to/your/trainee/bin:$PATH"
```

#### Bash completion

To enable tab completion for the `trainee` command in Bash, add the following
to your `~/.bash_profile`:

```bash
source ~/path/to/your/trainee/support/trainee.bash
```

### Workflow

#### Mark relevant merge requests

Mark all relevant merge request with your favorite emoji 🌱(`:seedling:`).

#### Update draft document

Update the draft document with the relevant merge requests using the trainee
maintainer template.

```sh
trainee pull
```

#### Edit draft document

Edit the draft document completing the information.

```sh
trainee edit
```

#### Update trainee maintainer issue

Update the trainee maintainer issue with the draft content.

```sh
trainee push
```

#### Open trainee maintainer issue

Open the trainee maintainer issue in the browser.

```sh
trainee open
```


## Who made this?

This was made by Arturo Herrero under the MIT License. You can also find me on
Twitter [@ArturoHerrero][5].


[1]: https://gitlab.com/splattael/traintainer
[2]: https://gitlab.com/nolith/review-tanuki
[3]: https://gitlab.com/gitlab-com/www-gitlab-com/issues/6571
[4]: https://gitlab.com/gitlab-com/www-gitlab-com/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=trainee%20maintainer
[5]: https://twitter.com/ArturoHerrero/
