require "gitlab"

require_relative "trainee/template"
require_relative "trainee/draft"
require_relative "trainee/pull"
require_relative "trainee/push"
require_relative "trainee/help"
